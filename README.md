# README #



### What is this repository for? ###

*This project is to provide the latest binary release for Allegro 5 and MinGW-W64 on the Windows platform*

*In addition, there are binaries available for the latest version of the discontinued Allegro 4.4.3 line*

**Build guide for Allegro 5**

You can find my step by step guide for compiling Allegro 5 from GIT or source release on my allegro member website at the following url :

http://members.allegro.cc/EdgarReynaldo/BuildA5.html

### How do I get set up? ###

*Go to the downloads page for all available files*

https://bitbucket.org/bugsquasher/unofficial-allegro-5-binaries/downloads/ 

Questions or problems may be directed to edgarreynaldo at members dot allegro dot cc. I will provide fixes and updates as necessary.

### Having trouble finding the compiler I used? ###

*Links provided here*

Simply match the mingw-w64 version with the version of the compiler used to make the binaries. For example, go to :

https://sourceforge.net/projects/mingw-w64/files/

and scroll down. Find the matching compiler below, or download directly here :

[i686-8.1.0-release-posix-dwarf-rt_v6-rev0.7z](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/8.1.0/threads-posix/dwarf/i686-8.1.0-release-posix-dwarf-rt_v6-rev0.7z)

[i686-7.2.0-release-posix-dwarf-rt_v5-rev1.7z](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/7.2.0/threads-posix/dwarf/i686-7.2.0-release-posix-dwarf-rt_v5-rev1.7z/download)
